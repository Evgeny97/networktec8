package lab8;

import lab8.my_socket_channel.MyChannelType;
import lab8.my_socket_channel.MySocketChannel;
import lab8.my_socket_channel.exceptions.BadRequestException;
import lab8.my_socket_channel.exceptions.NotImplementedException;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

public class PortForvarder {
    private ArrayList<MySocketChannel> mySocketChannels = new ArrayList<>();
    private ArrayList<MySocketChannel> mySocketChannelsOnRemove = new ArrayList<>();
    private Selector selector;
    private int lport = 10080;
    //private int rport = 80;
    // piwigo
    //private String rhost = "87.98.147.22";
    //picabu
    //private String rhost = "212.224.112.193";
    //private SocketAddress serverSocketAddress = new InetSocketAddress(rhost, rport);
    private ServerSocketChannel serverSocketChannel;
    //private boolean firstRequest = false;


    public void work() {
        try {
            selector = Selector.open();
            serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.bind(new InetSocketAddress(lport));
            serverSocketChannel.configureBlocking(false);
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

            while (true) {
//                try {
//                    Thread.sleep(500);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                int num = selector.select();
                System.out.println("NEXT SELECT " + num);

                Set selectedKeys = selector.selectedKeys();
                Iterator it = selectedKeys.iterator();
                while (it.hasNext()) {
                    SelectionKey curKey = (SelectionKey) it.next();
                    it.remove();
                    if ((curKey.isAcceptable())) {
                        startConnection();
                    } else {

                        SocketChannel curSocketChannel = (SocketChannel) curKey.channel();
                        MySocketChannel mySocketChannel = findAssociatedMySocketChannel(curSocketChannel);

                        if (curKey.isConnectable()) {
                            try {
                                if (curSocketChannel.finishConnect()) {
                                    System.out.println("finish connect");
                                    if (mySocketChannel.isOutputBufferEmpty()) {
                                        curKey.interestOps(SelectionKey.OP_READ);
                                    } else {
                                        curKey.interestOps(SelectionKey.OP_READ | SelectionKey.OP_WRITE);
                                    }
                                }
                            } catch (IOException e) {
                                System.out.println("EXCEPTION: " + e.getMessage());
                                removeChannel(mySocketChannel);
                                removeChannel(mySocketChannel.getPairChanel());
                            }
                        }
                        if (curKey.isWritable()) {
                            try {
                                writeData(mySocketChannel);
                            } catch (BadRequestException e) {
                                badRequestError(mySocketChannel.getPairChanel());
                            } catch (NotImplementedException e) {
                                notImplementedError(mySocketChannel.getPairChanel());
                            }
                        }
                        if (curKey.isReadable()) {
                            try {
                                readData(mySocketChannel);
                            } catch (BadRequestException e) {
                                badRequestError(mySocketChannel);
                            } catch (NotImplementedException e) {
                                notImplementedError(mySocketChannel);
                            } catch (IOException e) {
                                System.out.println("READ EXCEPTION:" + e.getMessage());
                                removeChannel(mySocketChannel);
                                removeChannel(mySocketChannel.getPairChanel());
                            }
                        }

                    }
                }
                removeFromList();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void notImplementedError(MySocketChannel mySocketChannel) {
        try {
            System.out.println("NotImplemented");
            mySocketChannel.writeToBufferFromByteArray("HTTP 501 NOT IMPLEMENTED\r\n\r\n".getBytes(StandardCharsets.ISO_8859_1));
            mySocketChannel.addToInterestOps(selector, SelectionKey.OP_WRITE);
            mySocketChannel.setNeedToCloseAfterWrite();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void badRequestError(MySocketChannel mySocketChannel) {
        try {
            System.out.println("BAD GATEWAY");
            mySocketChannel.writeToBufferFromByteArray("HTTP 502 BAD GATEWAY\r\n\r\n".getBytes(StandardCharsets.ISO_8859_1));
            mySocketChannel.addToInterestOps(selector, SelectionKey.OP_WRITE);
            mySocketChannel.setNeedToCloseAfterWrite();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private MySocketChannel findAssociatedMySocketChannel(SocketChannel curSocketChannel) {
        MySocketChannel mySocketChannel = null;
        for (MySocketChannel msc : mySocketChannels) {
            if (msc.isItYourChannel(curSocketChannel)) {
                mySocketChannel = msc;
                break;
            }
        }
        return mySocketChannel;
    }


    private void writeData(MySocketChannel mySocketChannel) throws IOException, BadRequestException, NotImplementedException {
        MySocketChannel pairChannel = mySocketChannel.getPairChanel();
        if (mySocketChannel.isBothBuffersEmpty()) {
            mySocketChannel.removeInterestOps(selector, SelectionKey.OP_WRITE);
            return;
        }
        mySocketChannel.writeFromBufferToSocketChannel();
        if (mySocketChannel.isBothBuffersFull()) {
            return;
        }
        // в буфере есть свободное место
        if (!pairChannel.isEof()) {
            pairChannel.addToInterestOps(selector, SelectionKey.OP_READ);
        }
        if (mySocketChannel.isBothBuffersEmpty()) {
            System.out.println("отписка с записи");
            mySocketChannel.removeInterestOps(selector, SelectionKey.OP_WRITE);
            if (mySocketChannel.isNeedToCloseAfterWrite()) {
                removeChannel(mySocketChannel);
                removeChannel(mySocketChannel.getPairChanel());
            }
        }
    }


    private void readData(MySocketChannel mySocketChannel) throws IOException, BadRequestException, NotImplementedException {
        MySocketChannel pairChannel = mySocketChannel.getPairChanel();
//        while (true) {
        int readBytes = 0;

        readBytes = pairChannel.writeToBufferFromChannel(mySocketChannel);
        if (pairChannel.isConnected()) {
            pairChannel.writeFromBufferToSocketChannel();
        } else {
            System.out.println("not yet connected so no write data");
            if (!pairChannel.isConnectionStarted()) {
                connectToServer(pairChannel);
            }
        }

        if (readBytes == -1) {
            System.out.println("GET -1 " + mySocketChannel.getType());
            mySocketChannel.setEof(true);
        }
        if (!pairChannel.isBothBuffersEmpty()) {
            if (pairChannel.isConnected()) {
                System.out.println("подписка на запись");
                pairChannel.addToInterestOps(selector, SelectionKey.OP_WRITE);
            }
        }
        System.out.println(mySocketChannel.getType() + " read: " + readBytes);
//        pairChannel.printInputBuffer();
//        pairChannel.printOutputBuffer();
        if (mySocketChannel.isEof()) {
            if (pairChannel.isEof() && mySocketChannel.isBothBuffersEmpty() &&
                    pairChannel.isBothBuffersEmpty()) {
                removeChannel(mySocketChannel);
                removeChannel(pairChannel);
//                    break;
            } else if (pairChannel.isBothBuffersEmpty()) {
                pairChannel.sendEof();
                mySocketChannel.removeInterestOps(selector, SelectionKey.OP_READ);
//                    break;
            }
        }
        if (pairChannel.isBothBuffersFull()) {
            mySocketChannel.removeInterestOps(selector, SelectionKey.OP_READ);
        }

//        }
    }

    private void removeChannel(MySocketChannel mySocketChannel) {
        if (mySocketChannel != null && !mySocketChannelsOnRemove.contains(mySocketChannel)) {
            System.out.println("add to remove");
            mySocketChannelsOnRemove.add(mySocketChannel);
        }
    }

    private void removeFromList() throws IOException {
        for (MySocketChannel mySocketChannel : mySocketChannelsOnRemove) {
            mySocketChannel.close();
        }
        mySocketChannels.removeAll(mySocketChannelsOnRemove);
        mySocketChannelsOnRemove.clear();
    }

    private void startConnection() throws IOException {
        System.out.println("new Accept");
        SocketChannel clientChannel = serverSocketChannel.accept();
        clientChannel.configureBlocking(false);
        SocketChannel serverChannel = SocketChannel.open();
        serverChannel.configureBlocking(false);

        MySocketChannel myClientChannel = new MySocketChannel(clientChannel, MyChannelType.CLIENT);
        MySocketChannel myServerChannel = new MySocketChannel(serverChannel, MyChannelType.SERVER,
                myClientChannel, false);
        myClientChannel.setPairChanel(myServerChannel);
        mySocketChannels.add(myClientChannel);
        mySocketChannels.add(myServerChannel);
        clientChannel.register(selector, SelectionKey.OP_READ);
    }

    private void connectToServer(MySocketChannel mySocketChannel) throws IOException {
        InetSocketAddress pairChannelInetSocketAddress = mySocketChannel.getInetSocketAddress();
        if (!mySocketChannel.isConnectionStarted() && pairChannelInetSocketAddress != null) {
            if (mySocketChannel.connectToServer()) {
                mySocketChannel.registerSocketChanel(selector, SelectionKey.OP_READ);
                System.out.println("instant connection");
            } else {
                mySocketChannel.registerSocketChanel(selector, SelectionKey.OP_CONNECT);
            }
        }
    }
}
