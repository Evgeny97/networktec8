package lab8.my_socket_channel;

import lab8.my_socket_channel.exceptions.BadRequestException;
import lab8.my_socket_channel.exceptions.NotImplementedException;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;

public class MySocketChannel {

    private SocketChannel socketChannel;
    private MyChannelType type;
    //    private CurrentBufferMode currentMode;
    private MySocketChannel pairChanel;
    private ByteBuffer inputByteBuffer;
    private ByteBuffer outputByteBuffer;
    private boolean eof;
    private boolean isConnectionStarted;
    private boolean needToCloseAfterWrite;
    private Parser parser;

    public MySocketChannel(SocketChannel socketChannel, MyChannelType type, boolean isConnectionStarted) {
        this(socketChannel, type, null, isConnectionStarted);
    }

    public MySocketChannel(SocketChannel socketChannel, MyChannelType type) {
        this(socketChannel, type, null, true);
    }

    public MySocketChannel(SocketChannel socketChannel, MyChannelType type, MySocketChannel pairChanel, boolean isConnectionStarted) {
        this.socketChannel = socketChannel;
        this.type = type;
        this.pairChanel = pairChanel;
        eof = false;
        needToCloseAfterWrite = false;
        this.isConnectionStarted = isConnectionStarted;
        inputByteBuffer = ByteBuffer.allocate(8192);
        outputByteBuffer = ByteBuffer.allocate(8192);
        if (type == MyChannelType.CLIENT) {
            parser = new ClientParser(this);
        } else {
            parser = new ServerParser(this);
        }
    }

    public boolean isNeedToCloseAfterWrite() {
        return needToCloseAfterWrite;
    }
    //buffers always in write to buffer state

    public void setNeedToCloseAfterWrite() {
        this.needToCloseAfterWrite = true;
    }

    public boolean connectToServer() throws IOException {

        InetSocketAddress inetSocketAddress = getInetSocketAddress();
        if (inetSocketAddress == null) {
            throw new IOException(" inetSocketAddress NOT ready");
        }
        boolean connect = socketChannel.connect(inetSocketAddress);
        isConnectionStarted = true;
        return connect;
    }

    public boolean isConnectionStarted() {
        return isConnectionStarted;
    }

    public void setConnectionStarted() {
        isConnectionStarted = true;
    }

    ByteBuffer getInputByteBuffer() {
        return inputByteBuffer;
    }

    ByteBuffer getOutputByteBuffer() {
        return outputByteBuffer;
    }

    public boolean isEof() {
        return eof;
    }

    public void setEof(boolean eof) {
        this.eof = eof;
    }

    public int writeToBufferFromChannel(MySocketChannel mySocketChannel) throws IOException
            , BadRequestException, NotImplementedException {
        int read = mySocketChannel.socketChannel.read(inputByteBuffer);
        parser.parseAndWriteToOutputBuffer();
        return read;
    }

    public void writeToBufferFromByteArray(byte[] bytes) throws IOException {
        outputByteBuffer.put(bytes);
    }

    public boolean isConnected() {
        return socketChannel.isConnected();
    }

    public void close() throws IOException {
        socketChannel.close();
    }

    public void printOutputBuffer() {
        System.out.println("OUTPUT BUFFER:");
        System.out.println(new String(outputByteBuffer.array(), StandardCharsets.ISO_8859_1));
    }

    public void printInputBuffer() {
        System.out.println("INPUT BUFFER:");
        System.out.println(new String(inputByteBuffer.array(), StandardCharsets.ISO_8859_1));
    }

    public int writeFromBufferToSocketChannel() throws IOException, BadRequestException, NotImplementedException {
        outputByteBuffer.flip();
        int wroteBytes = socketChannel.write(outputByteBuffer);
        outputByteBuffer.compact();
        parser.parseAndWriteToOutputBuffer();
        return wroteBytes;
    }

    public InetSocketAddress getInetSocketAddress() {
        return parser.getInetSocketAddress();
    }

    public void sendEof() throws IOException {
        if (socketChannel.isConnected()) {
            socketChannel.shutdownOutput();
        }
    }

    public int getInterestOps(Selector selector) {
        return socketChannel.keyFor(selector).interestOps();
    }

    public void setInterestOps(Selector selector, int ops) {
        socketChannel.keyFor(selector).interestOps(ops);
    }

    public void addToInterestOps(Selector selector, int ops) {
        SelectionKey selectionKey = socketChannel.keyFor(selector);
        if (selectionKey == null) {
            return;
        }
        selectionKey.interestOps(selectionKey.interestOps() | ops);
    }

    public void removeInterestOps(Selector selector, int ops) {
        SelectionKey selectionKey = socketChannel.keyFor(selector);
        if (selectionKey == null) {
            return;
        }
        selectionKey.interestOps(selectionKey.interestOps() & ~ops);
    }

    public boolean isInputBufferFull() {
        return inputByteBuffer.remaining() == 0;
    }

    public boolean isInputBufferEmpty() {
        return inputByteBuffer.position() == 0;
    }

    public boolean isOutputBufferFull() {
        return outputByteBuffer.remaining() == 0;
    }

    public boolean isOutputBufferEmpty() {
        return outputByteBuffer.position() == 0;
    }

    public boolean isBothBuffersFull() {
        return isOutputBufferFull() && isInputBufferFull();
    }

    public boolean isBothBuffersEmpty() {
        return isOutputBufferEmpty() && isInputBufferEmpty();
    }

    public boolean isItYourChannel(SocketChannel socketChannel) {
        return socketChannel == this.socketChannel;
    }

    public void registerSocketChanel(Selector selector, int ops) throws ClosedChannelException {
        socketChannel.register(selector, ops);
    }

    public MyChannelType getType() {
        return type;
    }

    public MySocketChannel getPairChanel() {
        return pairChanel;
    }

    public void setPairChanel(MySocketChannel pairChanel) {
        this.pairChanel = pairChanel;
    }
}

