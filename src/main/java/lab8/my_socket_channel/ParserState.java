package lab8.my_socket_channel;
//Created by Evgeny on 18.12.2017.

public enum ParserState {
    FIRST_LINE,HEADERS,CONNECTION_HEADER,BODY
}
