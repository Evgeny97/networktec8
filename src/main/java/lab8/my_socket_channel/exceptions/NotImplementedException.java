package lab8.my_socket_channel.exceptions;
//Created by Evgeny on 20.12.2017.

public class NotImplementedException extends MySocketChannelException{
    public NotImplementedException() {
    }

    public NotImplementedException(String message) {
        super(message);
    }
}
