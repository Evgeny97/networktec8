package lab8.my_socket_channel.exceptions;
//Created by Evgeny on 20.12.2017.

public class MySocketChannelException extends Exception {
    public MySocketChannelException() {
    }

    public MySocketChannelException(String message) {
        super(message);
    }
}
