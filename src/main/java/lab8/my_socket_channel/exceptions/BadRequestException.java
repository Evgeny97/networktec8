package lab8.my_socket_channel.exceptions;
//Created by Evgeny on 20.12.2017.

public class BadRequestException  extends MySocketChannelException{
    public BadRequestException() {
    }

    public BadRequestException(String message) {
        super(message);
    }
}
