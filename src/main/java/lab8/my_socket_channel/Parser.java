package lab8.my_socket_channel;
//Created by Evgeny on 21.12.2017.

import lab8.my_socket_channel.exceptions.BadRequestException;
import lab8.my_socket_channel.exceptions.NotImplementedException;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import static lab8.my_socket_channel.ParserState.*;

public abstract class Parser {
    protected MySocketChannel mySocketChannel;
    protected ParserState parserState;
    protected int alreadyPutBytesInOutputBuffer;
    protected boolean foundConnectionHeader;
    protected byte[] lastParsedBytes = null;

    public Parser(MySocketChannel mySocketChannel) {
        foundConnectionHeader = false;
        this.mySocketChannel = mySocketChannel;
        alreadyPutBytesInOutputBuffer = 0;
        parserState = FIRST_LINE;
    }

    public abstract InetSocketAddress getInetSocketAddress();

    //   abstract void parseAndWriteToOutputBuffer() throws BadRequestException, NotImplementedException;

    protected void writeToOutputBuffer() {
        ByteBuffer outputByteBuffer = mySocketChannel.getOutputByteBuffer();
        if (outputByteBuffer.hasRemaining()) {
            int remaining = outputByteBuffer.remaining();
            if (remaining < lastParsedBytes.length - alreadyPutBytesInOutputBuffer) {
                outputByteBuffer.put(lastParsedBytes, alreadyPutBytesInOutputBuffer, remaining);
                alreadyPutBytesInOutputBuffer += remaining;
            } else {
                outputByteBuffer.put(lastParsedBytes, alreadyPutBytesInOutputBuffer,
                        lastParsedBytes.length - alreadyPutBytesInOutputBuffer);
                alreadyPutBytesInOutputBuffer = 0;
                lastParsedBytes = null;
            }
        }
    }

    protected void removeFromInputBuffer(int firstBytesToRemove) {
        if (firstBytesToRemove > 0) {
            ByteBuffer inputByteBuffer = mySocketChannel.getInputByteBuffer();
            inputByteBuffer.flip();
            inputByteBuffer.position(firstBytesToRemove);
            inputByteBuffer.compact();
        }

    }

    void parseAndWriteToOutputBuffer() throws BadRequestException, NotImplementedException {
        if (lastParsedBytes != null) {
            writeToOutputBuffer();
        }

        if ((parserState == FIRST_LINE | parserState == HEADERS) && lastParsedBytes == null) {
            byte[] bytes = getBytesFromInputStream();
            String bytesInString = new String(bytes, StandardCharsets.ISO_8859_1);
            String[] lines = bytesInString.split("\r\n",-1);
            StringBuilder parsedOutputString = new StringBuilder();
            int parsedBytes = 0;
//            int indexOfEol = 0;
//            int indexOfPreviousEol = 0;
            // indexOfEol = bytesInString.indexOf("\r\n", indexOfEol);


            for (int i = 0; i < lines.length-1; i++) {
                String line = lines[i];

//            }
//            while (indexOfEol > 0) {
//                String line = bytesInString.substring(indexOfPreviousEol, indexOfEol);
//                indexOfPreviousEol = indexOfEol;
//                indexOfEol = bytesInString.indexOf("\r\n", indexOfEol + 1);
                if (parserState == FIRST_LINE) {
                    parsedOutputString.append(parseFirstLine(line)).append("\r\n");
                    parsedBytes += (line + "\r\n").getBytes(StandardCharsets.ISO_8859_1).length;
                } else {
                    String parsedLine = parseHeaders(line);
                    parsedBytes += (line + "\r\n").getBytes(StandardCharsets.ISO_8859_1).length;
                    if (parsedLine != null) {
                        parsedOutputString.append(parsedLine).append("\r\n");
                    } else {
                        if (!foundConnectionHeader) {
                            parsedOutputString.append("Connection: close\r\n");
                            parserState = BODY;
                        }
                        parsedOutputString.append("\r\n");
                        break;
                    }
                }
            }
            if (parsedOutputString.length() > 0) {
                lastParsedBytes = parsedOutputString.toString().getBytes(StandardCharsets.ISO_8859_1);
                writeToOutputBuffer();
                removeFromInputBuffer(parsedBytes);
            }

        }
        if (parserState == BODY && lastParsedBytes == null) {
            byte[] bytes = getBytesFromInputStream();
            lastParsedBytes = bytes;
            writeToOutputBuffer();
            removeFromInputBuffer(bytes.length);
        }
        //  throw new RuntimeException("can't get there");
    }

    protected abstract String parseFirstLine(String inputString) throws BadRequestException, NotImplementedException;

    private String parseHeaders(String inputString) {
        if (inputString.equals("")) {
            parserState = BODY;
            return null;
        }
        //String[] split = inputString.split(":", 2);
        if (inputString.startsWith("Connection:")) {
            foundConnectionHeader = true;
            return "Connection: close";
        }
        return inputString;
    }

    private byte[] getBytesFromInputStream() {
        return Arrays.copyOfRange(mySocketChannel.getInputByteBuffer().array(), 0,
                mySocketChannel.getInputByteBuffer().position());
    }
}
