package lab8.my_socket_channel;
//Created by Evgeny on 18.12.2017.

import lab8.my_socket_channel.exceptions.BadRequestException;
import lab8.my_socket_channel.exceptions.NotImplementedException;

import java.net.InetSocketAddress;

public class ClientParser extends Parser {


    ClientParser(MySocketChannel mySocketChannel) {
        super(mySocketChannel);
    }

    @Override
    public InetSocketAddress getInetSocketAddress() {
        System.out.println("не должно быть тут");
        return null;
    }

    @Override
    protected String parseFirstLine(String inputString) throws BadRequestException, NotImplementedException {
        parserState = ParserState.HEADERS;
        return inputString;
    }


}
