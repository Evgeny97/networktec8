package lab8.my_socket_channel;
//Created by Evgeny on 21.12.2017.

import lab8.my_socket_channel.exceptions.BadRequestException;
import lab8.my_socket_channel.exceptions.NotImplementedException;

import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;

import static lab8.my_socket_channel.ParserState.BODY;
import static lab8.my_socket_channel.ParserState.FIRST_LINE;
import static lab8.my_socket_channel.ParserState.HEADERS;

public class ServerParser extends Parser{
    private int port;
    private String host;
    public ServerParser(MySocketChannel mySocketChannel) {
        super(mySocketChannel);
        host = null;
    }

    @Override
    public InetSocketAddress getInetSocketAddress() {
        if (host != null) {
            return new InetSocketAddress(host, port);
        }
        return null;
    }


    @Override
    protected String parseFirstLine(String inputString) throws BadRequestException, NotImplementedException {
        int indexOfFirstSpace = inputString.indexOf(" ");
        if (indexOfFirstSpace == -1) {
            throw new BadRequestException();
        }
        String method = inputString.substring(0, indexOfFirstSpace);
        System.out.println("Method = " + method);
        int indexOfSecondSpace = inputString.indexOf(" ", indexOfFirstSpace + 1);
        if (indexOfSecondSpace == -1) {
            throw new BadRequestException();
        }
        String protocolAndUrl = inputString.substring(indexOfFirstSpace + 1, indexOfSecondSpace);
        String protocolPrefix = "http://";
        if (!protocolAndUrl.startsWith(protocolPrefix)) {
            throw new NotImplementedException();
        }
        int indexOfSlash = protocolAndUrl.indexOf("/", protocolPrefix.length() + 1);
        if (indexOfSlash == -1) {
            throw new BadRequestException();
        }
        String hostAndPort = protocolAndUrl.substring(protocolPrefix.length(),
                indexOfSlash);
        String pathAndQuery = protocolAndUrl.substring(indexOfSlash);
        port = 80;
        host = hostAndPort;
        int indexOfDots = hostAndPort.indexOf(":");
        if (indexOfDots != -1) {
            String[] split = hostAndPort.split(":");
            host = split[0];
            port = Integer.valueOf(split[1]);
        }
        String httpProtocolVersion = inputString.substring(indexOfSecondSpace + 1);
        String result = method + " " + pathAndQuery + " " + httpProtocolVersion;
        System.out.println("result = " + result);
        parserState = ParserState.HEADERS;
        return result;
    }




}
